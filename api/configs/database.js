module.exports = {
    // URL : 'pg://postgres:123456@localhost:5432/publicidadedigital', // database url local!
    URL: 'postgres://nzlnjkdp:YG3tLi-1ZdpSTnlK72qfFzvWLOYLLUhk@horton.elephantsql.com:5432/nzlnjkdp',
    DATABASE_NAME: 'DATABASE', // database name
    DATABASE_USERNAME: 'USERNAME', // database username
    DATABASE_PASSWORD: 'PASSWORD', // database password
    DATABASE_HOST: 'HOST', // database host
    DATABASE_PORT: 5432, // database port
    MAX_CLIENTS: 50, // max number of clients in the pool
    IDLE_TIMEOUT: 30000 // how long a client is allowed to remain idle before being closed
};
