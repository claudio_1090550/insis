INSERT INTO campanha (descricao, status, id, nome) VALUES ('Campanha 1', 'Iniciada', 1, 'Campanha 1');

INSERT INTO canal (designacao, id) VALUES ('Televisao', 1);
INSERT INTO canal (designacao, id) VALUES ('Facebook', 2);
INSERT INTO canal (designacao, id) VALUES ('Email', 3);
INSERT INTO canal (designacao, id) VALUES ('SMS', 4);
INSERT INTO canal (designacao, id) VALUES ('Instagram', 5);

INSERT INTO campanha_canal (id, idcampanha, idcanal, descricao, conteudo) VALUES (1, 1, 1, 'Descricao 1', 'Aqui o conteudo 1!');
INSERT INTO campanha_canal (id, idcampanha, idcanal, descricao, conteudo) VALUES (2, 1, 2, 'Descricao 2', 'Aqui o conteudo 2!');

INSERT INTO pais (nome, fuso_horario, id) VALUES ('Portugal', 'GMT+1', 1);
INSERT INTO pais (nome, fuso_horario, id) VALUES ('Brasil', 'GMT+8', 2);

INSERT INTO destinatario (idade, idpais, id) VALUES (30, 1, 1);
INSERT INTO destinatario (idade, idpais, id) VALUES (25, 2, 2);
INSERT INTO destinatario (idade, idpais, id) VALUES (28, 1, 3);
INSERT INTO destinatario (idade, idpais, id) VALUES (40, 1, 4);

INSERT INTO destinatario_campanha (idcampanha, iddestinatario, status, id) VALUES (1, 1, 'Aguarda envio', 1);
INSERT INTO destinatario_campanha (idcampanha, iddestinatario, status, id) VALUES (1, 3, 'Aguarda Envio', 2);

INSERT INTO destinatario_canal (idcanal, endereco, consentimento, id, iddestinatario) VALUES (2, 'Endereco 1', 'dsdsd', 2, 2);
INSERT INTO destinatario_canal (idcanal, endereco, consentimento, id, iddestinatario) VALUES (1, 'Endereco 2', 'sdsdsd', 1, 1);
INSERT INTO destinatario_canal (idcanal, endereco, consentimento, id, iddestinatario) VALUES (2, 'Endereco 3', 'sfsdf', 4, 3);
INSERT INTO destinatario_canal (idcanal, endereco, consentimento, id, iddestinatario) VALUES (3, 'Endereco 4', 'sdsd', 3, 1);

INSERT INTO erros (descricao, data, idcampanha, id) VALUES ('Ocorreu um erro na criação da campanha.', '2018-03-18', NULL, 1);

SELECT pg_catalog.setval('campanha_canal_idseq', 2, true);
SELECT pg_catalog.setval('campanha_idseq', 1, true);
SELECT pg_catalog.setval('canal_idseq', 5, true);
SELECT pg_catalog.setval('destinatario_campanha_idseq', 2, true);
SELECT pg_catalog.setval('destinatario_canal_idseq', 4, true);
SELECT pg_catalog.setval('destinatario_idseq', 4, true);
SELECT pg_catalog.setval('erros_idseq', 1, true);
SELECT pg_catalog.setval('pais_idseq', 2, true);
