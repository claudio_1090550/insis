-- CREATE TABLE

CREATE TABLE campanha (
    id bigint NOT NULL,
    descricao text,
    status text,
    nome text
);
ALTER TABLE campanha OWNER TO nzlnjkdp;

CREATE TABLE campanha_canal (
    id bigint NOT NULL,
    idcampanha integer,
    idcanal integer,
    descricao text,
    conteudo text
);
ALTER TABLE campanha_canal OWNER TO nzlnjkdp;

CREATE SEQUENCE campanha_canal_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE campanha_canal_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE campanha_canal_idseq OWNED BY campanha_canal.id;

CREATE SEQUENCE campanha_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE campanha_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE campanha_idseq OWNED BY campanha.id;

CREATE TABLE canal (
    designacao text,
    id bigint NOT NULL
);
ALTER TABLE canal OWNER TO nzlnjkdp;

CREATE SEQUENCE canal_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE canal_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE canal_idseq OWNED BY canal.id;

CREATE TABLE destinatario (
    idade integer,
    idpais integer,
    id bigint NOT NULL
);
ALTER TABLE destinatario OWNER TO nzlnjkdp;

CREATE TABLE destinatario_campanha (
    idcampanha integer,
    iddestinatario integer,
    status text,
    id bigint NOT NULL
);
ALTER TABLE destinatario_campanha OWNER TO nzlnjkdp;

CREATE SEQUENCE destinatario_campanha_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE destinatario_campanha_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE destinatario_campanha_idseq OWNED BY destinatario_campanha.id;

CREATE TABLE destinatario_canal (
    idcanal integer,
    endereco text,
    consentimento text,
    id bigint NOT NULL,
    iddestinatario integer
);
ALTER TABLE destinatario_canal OWNER TO nzlnjkdp;

CREATE SEQUENCE destinatario_canal_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE destinatario_canal_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE destinatario_canal_idseq OWNED BY destinatario_canal.id;


CREATE SEQUENCE destinatario_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE destinatario_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE destinatario_idseq OWNED BY destinatario.id;

CREATE TABLE erros (
    descricao text,
    data date,
    idcampanha integer,
    id bigint NOT NULL
);
ALTER TABLE erros OWNER TO nzlnjkdp;

CREATE SEQUENCE erros_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE erros_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE erros_idseq OWNED BY erros.id;


CREATE TABLE pais (
    nome text,
    fuso_horario text,
    id bigint NOT NULL
);
ALTER TABLE pais OWNER TO nzlnjkdp;

CREATE SEQUENCE pais_idseq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
ALTER TABLE pais_idseq OWNER TO nzlnjkdp;
ALTER SEQUENCE pais_idseq OWNED BY pais.id;


ALTER TABLE ONLY campanha ALTER COLUMN id SET DEFAULT nextval('campanha_idseq'::regclass);

ALTER TABLE ONLY campanha_canal ALTER COLUMN id SET DEFAULT nextval('campanha_canal_idseq'::regclass);

ALTER TABLE ONLY canal ALTER COLUMN id SET DEFAULT nextval('canal_idseq'::regclass);

ALTER TABLE ONLY destinatario ALTER COLUMN id SET DEFAULT nextval('destinatario_idseq'::regclass);

ALTER TABLE ONLY destinatario_campanha ALTER COLUMN id SET DEFAULT nextval('destinatario_campanha_idseq'::regclass);

ALTER TABLE ONLY destinatario_canal ALTER COLUMN id SET DEFAULT nextval('destinatario_canal_idseq'::regclass);

ALTER TABLE ONLY erros ALTER COLUMN id SET DEFAULT nextval('erros_idseq'::regclass);

ALTER TABLE ONLY pais ALTER COLUMN id SET DEFAULT nextval('pais_idseq'::regclass);

-- PK's

ALTER TABLE ONLY canal
    ADD CONSTRAINT id_pk PRIMARY KEY (id);

ALTER TABLE ONLY destinatario_campanha
    ADD CONSTRAINT "pkDestCamp" PRIMARY KEY (id);

ALTER TABLE ONLY campanha_canal
    ADD CONSTRAINT "pk_CampanhaCanal" PRIMARY KEY (id);

ALTER TABLE ONLY destinatario
    ADD CONSTRAINT pk_destinatario PRIMARY KEY (id);

ALTER TABLE ONLY campanha
    ADD CONSTRAINT pk_id PRIMARY KEY (id);

ALTER TABLE ONLY pais
    ADD CONSTRAINT pk_pais PRIMARY KEY (id);

ALTER TABLE ONLY erros
    ADD CONSTRAINT pkid1 PRIMARY KEY (id);

ALTER TABLE ONLY destinatario_canal
    ADD CONSTRAINT pkid2 PRIMARY KEY (id);

-- Add indexes

CREATE INDEX "fki_fk_DestCampanha" ON public.destinatario_campanha USING btree (idcampanha);

CREATE INDEX fki_fk_canal ON public.destinatario_canal USING btree (idcanal);

CREATE INDEX fki_fk_destinatario ON public.destinatario_campanha USING btree (iddestinatario);

CREATE INDEX "fki_fk_idDest" ON public.destinatario_canal USING btree (iddestinatario);

CREATE INDEX fki_fk_pais ON public.destinatario USING btree (idpais);

CREATE INDEX fki_fkcampanhaerro ON public.erros USING btree (idcampanha);

-- FK's

ALTER TABLE ONLY destinatario_campanha
    ADD CONSTRAINT "fk_campaDest" FOREIGN KEY (idcampanha) REFERENCES campanha(id);

ALTER TABLE ONLY campanha_canal
    ADD CONSTRAINT fk_canal FOREIGN KEY (idcanal) REFERENCES canal(id);

ALTER TABLE ONLY campanha_canal
    ADD CONSTRAINT fk_campanha FOREIGN KEY (idcampanha) REFERENCES campanha(id);

ALTER TABLE ONLY destinatario_canal
    ADD CONSTRAINT "fk_canalD" FOREIGN KEY (idcanal) REFERENCES canal(id);

ALTER TABLE ONLY destinatario_campanha
    ADD CONSTRAINT "fk_destC" FOREIGN KEY (iddestinatario) REFERENCES destinatario(id);

ALTER TABLE ONLY destinatario_canal
    ADD CONSTRAINT "fk_idDest" FOREIGN KEY (iddestinatario) REFERENCES destinatario(id);

ALTER TABLE ONLY destinatario
    ADD CONSTRAINT "fk_paisDest" FOREIGN KEY (idpais) REFERENCES pais(id);

ALTER TABLE ONLY erros
    ADD CONSTRAINT fkcampanhaerro FOREIGN KEY (idcampanha) REFERENCES campanha(id);
