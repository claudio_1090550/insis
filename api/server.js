'use strict';

/**
* Publicidade Digital - API
**/

var Hapi = require('hapi');
var Good = require('good');
var Pg = require('pg');

var databaseConfig = require("./configs/database.js");
var server = new Hapi.Server();

server.connection({
    // host: '127.0.0.1',
    // port: 3002
    port: process.env.PORT || 3002
});


//GET: campanha/{x}; para leitura duma campanha
server.route({
    method: 'GET',
    path: '/campanha/{id}',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {
        console.log(request.headers.authorization);
        console.log(request);

        var client = new Pg.Client(databaseConfig.URL);
        var rows = [];

        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect();

        var sql = "SELECT descricao, status, id, nome FROM campanha WHERE id = (" + request.params.id + ")";
        var query = client.query(sql);

        query.on('end', function(result)
        { 
            reply(JSON.stringify(result.rows[0]));
        });
    }
});
// POST: campanha ; para criação duma nova campanha
server.route({
    method: 'POST',
    path: '/campanha/',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {
      var data = request.payload;
      console.log(data);

      var sql = "INSERT INTO campanha (descricao, status, nome) VALUES ('" + data.descricao +"','"+ data.status+"', '"+data.nome+"')";
      console.log(sql);

      var client = new Pg.Client(databaseConfig.URL);
      client.connect();
      
      var query = client.query(sql, data);

      client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished

      query.on('end', function()
      {
          console.log("Query Completed");
          var response = new Object();
          response.nome = data.nome;
          reply(response);
      });

    }
});

// PUT: campanha/{x}; para atualização do estado da campanha
server.route({
    method: 'PUT',
    path: '/campanha/{id}',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {

      var data = request.payload;
      console.log(data);
  
      var sql = "UPDATE campanha SET status = '"+ data.status +"', descricao = '" + data.descricao +"', nome = '" + data.nome + "' WHERE id = (" + request.params.id + ")";
      console.log(sql);
  
      var client = new Pg.Client(databaseConfig.URL);
      client.connect();
  
      var query = client.query(sql, data);
  
      client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
  
      query.on('end', function()
      {
          console.log("Query Completed");
          reply(JSON.stringify("Campanha atualizada"));
      });
  
    }
  });

// PUT: campanha/{idcampanha}/canal/{idcanal}; para atualização do estado do canal-campanha

server.route({
    method: 'PUT',
    path: '/campanha/{id_campanha}/canal/{id_canal}',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {
      var data = request.payload;
  
      console.log('#############');
      //console.log(data.pais);
      //console.log(data.idade);
      console.log('#############');
  
      var sql = "UPDATE campanha_canal SET periodo = '"+ data.periodo +"' WHERE idcanal = ('" + request.params.id_canal + "') AND idcampanha = ('" + request.params.id_campanha + "')";
  
      console.log(sql);
  
      var client = new Pg.Client(databaseConfig.URL);
      client.connect();
  
      var query = client.query(sql, data);
  
      client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
  
      query.on('end', function()
      {
        console.log("Query Completed");
        reply(JSON.stringify("Campanha atualizada"));
      });
  
    }
  });

// GET: campanha/{x}/conteúdo/canal/{y}; para leitura de informação do conteúdo por canal
server.route({
    method: 'GET',
    path: '/campanha/{x}/conteudo/canal/{y}',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {
        console.log(request.headers.authorization);
        console.log(request);

        var client = new Pg.Client(databaseConfig.URL);
        var rows = [];

        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect();

        // x = id campanha
        // y = id canal
        var sql = "SELECT id, idcampanha, idcanal, descricao, conteudo FROM campanha_canal WHERE idcampanha = (" + request.params.x + ") AND idcanal= (" + request.params.y + ")";
        
        var query = client.query(sql);

        query.on('end', function(result)
        {
            reply(JSON.stringify(result.rows[0]));
        });
    }
});

// GET: destinatário/{x}; para leitura de informação sobre o destinatario
server.route({
    method: 'GET',
    path: '/destinatario/{id}',
    config: {
        auth: false,
     cors: {
            origin: ['*'],
            additionalHeaders: ['cache-control', 'x-requested-with']
        }
    },
    handler: function (request, reply)
    {
        console.log(request.headers.authorization);
        console.log(request);

        var client = new Pg.Client(databaseConfig.URL);
        var rows = [];

        client.on('drain', client.end.bind(client)); //disconnect client when all queries are finished
        client.connect();

        var sql = "SELECT idade, idpais, id FROM destinatario WHERE id = (" + request.params.id + ")";

        var query = client.query(sql);

        query.on('end', function(result)
        {
            console.log("Query Completed");
            reply(JSON.stringify(result.rows[0]));
        });
    }
});

server.register([
    {
        register: Good,
        options: {
            reporters: {
                console: [{
                    module: 'good-squeeze',
                    name: 'Squeeze',
                    args: [{
                        response: '*',
                        log: '*'
                    }]
                }, {
                    module: 'good-console'
                }, 'stdout']
            }
        }
    },
    {
        register: require('hapi-cors'),
        options: {
                origins: ['http://localhost'],
                headers: ['Accept', 'Content-Type', 'Authorization']
        }
    }], (err) => {

        if (err) {
            throw err; // error when loading the plugin
        }

          server.start((err) => {

            if (err) {
               throw err;
            }
            server.log('info', 'Server running at: ' + server.info.uri);
        });
});
